section .text
%define EXIT_CODE 60
%define STDOUT 1
%define WRITE 1
%define WHITESPACE 0x20
%define SYS_BASE 10
%define ASCII2INT 48
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_CODE
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax,rax
    .loop:
	cmp byte [rdi+rax], 0
	je .end
	inc rax
	jmp .loop
    .end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string: 
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rax, WRITE
    mov rdi, STDOUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdx, 1
    mov rsi, rsp
    mov rax, WRITE
    mov rdi, STDOUT
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, '\n'
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi ; записываем число для дальнейшей работы
    dec rsp
    mov byte[rsp], 0; записваем нуль терминатор
    mov r9, 1 ; счетчик для того что бы вернуться к rsp
    mov r8, SYS_BASE ; CONST для деления числа
    .loop:
	xor rdx, rdx ; очистка rdx для правильной работы div()
	div r8 ; div(rax) -> rdx = rax % 10, rax = rax//10
	add rdx, ASCII2INT ; перевод в ASCI символ
	dec rsp 
	inc r9
	mov [rsp], dl; записываем байт в стек
	test rax, rax; проверка есть ли еще цифры
	jnz .loop
    mov rdi, rsp
    push rcx ; сохраняем регистр
    call print_string ; выводим строку цифр из стека
    pop rcx
    add rsp, r9 ; возвращаем rsp в начальное состояние
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jge .pos
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .pos:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx ; обнуляем счетчик
    .loop:
	mov al, [rdi+rcx] ; записываем символ из первой строки
	mov dl, [rsi+rcx] ; записываем символ из второй строки
	cmp al,dl 
	jne .neq ; если символы не равны возвращаем 0 и выходим
	cmp al, 0
	je .eq ; если символы равны и один из них нуль терминатор, то строки закончились и они равны
	inc rcx ; увеличиваем счетчик
	jmp .loop
    .neq: 
	mov rax, 0
	ret
    .eq:
	mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax ; rax <- 0 (read syscall)
    xor rdi, rdi ; rdi <- 0 (stdin)
    push 0       ; выделяем место под 1 символ
    mov rsi, rsp ; передаем ячейку куда сохранить результат
    mov rdx, 1   ; считать 1 символ
    syscall      
    pop rax      ; записываем в rax результат вызова
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi              ; rdi начало буфера
    mov r10, rdi             ; r10 начало буфера для получения длины слова
    mov r9, rsi              ; rsi размер буфера
    dec r9		     ; уменьшаем буфер т.к. нужно уместить нуль
    cmp r9, 1
    jb .error

    .skip_whitespaces:		     ; пропускаем все пробельные символы
        call read_char
	cmp rax, 0x20
        je .skip_whitespaces
        cmp rax, 0x9
        je .skip_whitespaces
        cmp rax, 0xA
        je .skip_whitespaces
        jmp .check_if_eof

    .check_if_eof:                  ; записываем слово
        cmp rax, 0           ; проверки на окончание слова
	je .eof_reached
        cmp rax, 0x20   
        je .eof_reached
        cmp rax, 0x9 
        je .eof_reached
        cmp rax, 0xA 
        je .eof_reached
        mov [r8], al         ; если очередной символ не пробельный добавляем его в слово
        dec r9               ; уменьшаем буфер
        inc r8               ; сдвигаем указатель
        cmp r9, 0            ; если слово = буферу то ошибка        
        je .error
        call read_char       ; читаем новый символ
        jmp .check_if_eof         

    .eof_reached:                  ; добавляем нуль терминатор и записываем нужные данные
	mov byte[r8], 0
        mov rdi, r10
	call string_length   ; считаем длину слова
	mov rdx, rax
	mov rax, r10
        ret

.error:
    xor rax, rax
    ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
push rbx
    xor rax, rax
    xor rdx, rdx
    mov r10, SYS_BASE             ; CONST для перевода чисел
    .loop:
	xor rbx,rbx
	cmp byte[rdi], '0'  ; если символ меньше 0 в ASCII то ошибка
	jb .error
	cmp byte[rdi], '9'  ; если символ больше 9 в ASCII то ошибка
	jg .error
	mov bl, byte[rdi]   ; записываем символ в младший байт rbx
	sub rbx, ASCII2INT         ; переводим символ из ASCII в число
	push rdx            ; сохраняем rdx, тк mul его изменит
	mul r10             ; сдвигаем порядок числа в результате
	pop rdx
	add rax, rbx        ; добавляем к результату наше число
	inc rdi
	inc rdx
	jmp .loop
    .error:
	pop rbx
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
push rbx
    xor rax, rax
    mov bl, byte[rdi]
    cmp bl, '-'          ; проверяем если первый символ '-'
    je .neg		 ; если так то переходим к обработке отрицательного числа
    call parse_uint
    pop rbx
    ret
    .neg:
	inc rdi          ; пропускаем первый символ и передаем указатель в функцию
	call parse_uint
	cmp rdx, 0       ; если результат функции 0, те число не прочитано выходим
	je .error 
	inc rdx          ; увеличиваем счетчик длины числа
	neg rax		 ; rax * (-1)
        pop rbx
	ret
    .error:
        pop rbx
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx
    .loop:
	cmp rdx, rcx ; если строка больше буфера то ошибка
	je .error
	cmp byte[rdi+rcx], 0
	je .end
	mov rax, [rdi+rcx] ; т.к. нельзя делать mov по 2 относительным адресам используем rax как буфер для символа
	mov [rsi+rcx], rax ; 
	inc rcx            
	jmp .loop
    .error:
	xor rax,rax
	ret
    .end:
	mov byte[rsi+rcx], 0
	mov rax, rcx	
	ret
